# assembly_comparison_analysis

Post workflow analysis scripts for generating figures and tables. This uses data output from the workflow in https://gitlab.com/ModernisingMedicalMicrobiology/assembly_comparison.

## Run analysis

To run the analysis, use this command.

```bash
python3 analysis.py
```

## Outputs

# contigs

Assembled contigs are in the [assemblies directory](assemblies/). 

The folders are

- [Canu](assemblies/standard_canu/)
- [Flye](assemblies/standard_assemble/)
- [Unicycler](assemblies/standard_unicycler/)
- [SPAdes](assemblies/standard_convertGraph_spades/)

# BLASTn results

The raw BLASTn results are in the [blastn directory](blastn/).
