#!/usr/bin/env python3
import os
import gzip
from Bio import SeqIO, pairwise2
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
sns.set_style('darkgrid')

samples=['Ecoli_r10',
        'Ecoli_r10.3_SUP',
        'Ecoli_r9',
        'Ecoli_r9_sup',
        'Ecoli_r10.4',
        'Ecoli_r10.4_sup',
        'Ecoli_r10.4_sup_dup',
        'Ecoli_r10.4_plex',
        'Ecoli_r10.4_plex_sup_dup',
        'Ecoli_r10.4_plex_sup_dup_hybrid',
        'Ecoli_r10.4_plex_sup_simp',
        'Kpneumo_r10',
        'Kpneumo_r10.3_SUP',
        'Kpneumo_r9',
        'Kpneumo_r9_sup',
        'Kpneumo_r10.4',
        'Kpneumo_r10.4_sup',
        'Kpneumo_r10.4_sup_dup',
        'Kpneumo_r10.4_plex',
        'Kpneumo_r10.4_plex_sup_dup',
        'Kpneumo_r10.4_plex_sup_dup_hybrid',
        'Kpneumo_r10.4_plex_sup_simp',
        'MRSA_r10',
        'MRSA_r10.3_SUP',
        'MRSA_r9',
        'MRSA_r9_sup',
        'MRSA_r10.4',
        'MRSA_r10.4_sup',
        'MRSA_r10.4_sup_dup',
        'MRSA_r10.4_plex',
        'MRSA_r10.4_plex_sup_dup',
        'MRSA_r10.4_plex_sup_dup_hybrid',
        'MRSA_r10.4_plex_sup_simp',
        'Pa01_r10',
        'Pa01_r10.3_SUP',
        'Pa01_r9',
        'Pa01_r9_sup',
        'Pa01_r10.4',
        'Pa01_r10.4_sup',
        'Pa01_r10.4_sup_dup',
        'Pa01_r10.4_plex',
        'Pa01_r10.4_plex_sup_dup',
        'Pa01_r10.4_plex_sup_dup_hybrid',
        'Pa01_r10.4_plex_sup_simp',
        'Ecoli_r1041_duplex',
        'Ecoli_r1041_simplex',
        'Pa01_BSA_r1041_duplex',
        'Pa01_BSA_r1041_simplex',
        'Kpneu_BSA_r1041_duplex',
        'Kpneu_BSA_r1041_simplex',
        'MRSA_BSA_r1041_duplex',
        'MRSA_BSA_r1041_simplex'
        ]

sampleDict={'Ecoli_r10':{'flowcell': 'r10.3','model':'Rerio', 'species':'E.coli', 
    'parent':'Ecoli_r10', 'plexed':False, 'run': 0},
        'Ecoli_r10.3_SUP':{'flowcell': 'r10.3','model':'SUP', 'species':'E.coli',
            'parent':'Ecoli_r10', 'plexed':False, 'run': 0},
        'Ecoli_r9':{'flowcell': 'r9.4.1','model':'HAC', 'species':'E.coli',
            'parent':'Ecoli_r9', 'plexed':True, 'run': 0},
        'Ecoli_r9_sup':{'flowcell': 'r9.4.1','model':'SUP', 'species':'E.coli',
            'parent':'Ecoli_r9', 'plexed':True, 'run': 0},
        'Ecoli_r10.4':{'flowcell': 'r10.4','model':'HAC', 'species':'E.coli',
            'parent':'Ecoli_r10.4', 'plexed':False, 'run': 0},
        'Ecoli_r10.4_sup':{'flowcell': 'r10.4','model':'SUP', 'species':'E.coli',
            'parent':'Ecoli_r10.4', 'plexed':False, 'run': 0},
        'Ecoli_r10.4_sup_dup':{'flowcell': 'r10.4','model':'DUPLEX', 'species':'E.coli',
            'parent':'Ecoli_r10.4', 'plexed':False, 'run': 0},
        'Ecoli_r10.4_plex':{'flowcell': 'r10.4','model':'HAC', 'species':'E.coli',
            'parent':'Ecoli_r10.4_plex', 'plexed':True, 'run': 1},
        'Ecoli_r10.4_plex_sup_dup':{'flowcell': 'r10.4','model':'DUPLEX', 'species':'E.coli',
            'parent':'Ecoli_r10.4_plex', 'plexed':True, 'run': 1},
        'Ecoli_r10.4_plex_sup_dup_hybrid':{'flowcell': 'r10.4','model':'DUPLEX+SIMPLEX', 'species':'E.coli',
            'parent':'Ecoli_r10.4_plex', 'plexed':True, 'run': 1},
        'Ecoli_r10.4_plex_sup_simp':{'flowcell': 'r10.4','model':'SUP', 'species':'E.coli',
            'parent':'Ecoli_r10.4_plex', 'plexed':True, 'run': 1},
        'Ecoli_r10.4_SUP_simplex':{'flowcell': 'r10.4','model':'SUP', 'species':'E.coli',
            'parent':'Ecoli_r10.4_SUP_simplex', 'plexed':False, 'run': 2},
        'Ecoli_r10.4_SUP_duplex':{'flowcell': 'r10.4','model':'DUPLEX', 'species':'E.coli',
            'parent':'Ecoli_r10.4_SUP_simplex', 'plexed':False, 'run': 2},
        'Ecoli_r10.4_SUP_SIM_3':{'flowcell': 'r10.4','model':'SUP', 'species':'E.coli',
            'parent':'Ecoli_r10.4_SUP_simplex', 'plexed':False, 'run': 2},
        'Ecoli_r10.4_SUP_DUP_3':{'flowcell': 'r10.4','model':'DUPLEX', 'species':'E.coli',
            'parent':'Ecoli_r10.4_SUP_simplex', 'plexed':False, 'run': 2},
        'Kpneumo_r10':{'flowcell': 'r10.3','model':'Rerio', 'species':'K.pneumoniae',
            'parent':'Kpneumo_r10', 'plexed':False, 'run': 0},
        'Kpneumo_r10.3_SUP':{'flowcell': 'r10.3','model':'SUP', 'species':'K.pneumoniae',
            'parent':'Kpneumo_r10', 'plexed':False, 'run': 0},
        'Kpneumo_r9':{'flowcell': 'r9.4.1','model':'HAC', 'species':'K.pneumoniae',
            'parent':'Kpneumo_r9', 'plexed':True, 'run': 0},
        'Kpneumo_r9_sup':{'flowcell': 'r9.4.1','model':'SUP', 'species':'K.pneumoniae',
            'parent':'Kpneumo_r9', 'plexed':True, 'run': 0},
        'Kpneumo_r10.4':{'flowcell': 'r10.4','model':'HAC', 'species':'K.pneumoniae',
            'parent':'Kpneumo_r10.4', 'plexed':False, 'run': 0},
        'Kpneumo_r10.4_sup':{'flowcell': 'r10.4','model':'SUP', 'species':'K.pneumoniae',
            'parent':'Kpneumo_r10.4', 'plexed':False, 'run': 0},
        'Kpneumo_r10.4_sup_dup':{'flowcell': 'r10.4','model':'DUPLEX', 'species':'K.pneumoniae',
            'parent':'Kpneumo_r10.4', 'plexed':False, 'run': 0},
        'Kpneumo_r10.4_plex':{'flowcell': 'r10.4','model':'HAC', 'species':'K.pneumoniae',
            'parent':'Kpneumo_r10.4_plex', 'plexed':True, 'run': 1},
        'Kpneumo_r10.4_plex_sup_dup':{'flowcell': 'r10.4','model':'DUPLEX', 'species':'K.pneumoniae',
            'parent':'Kpneumo_r10.4_plex', 'plexed':True, 'run': 1},
        'Kpneumo_r10.4_plex_sup_dup_hybrid':{'flowcell': 'r10.4','model':'DUPLEX+SIMPLEX', 'species':'K.pneumoniae',
            'parent':'Kpneumo_r10.4_plex', 'plexed':True, 'run': 1},
        'Kpneumo_r10.4_plex_sup_simp':{'flowcell': 'r10.4','model':'SUP', 'species':'K.pneumoniae',
            'parent':'Kpneumo_r10.4_plex', 'plexed':True, 'run': 1},
        'Kpneu_r10.4_SUP_simplex':{'flowcell': 'r10.4','model':'SUP', 'species':'K.pneumoniae',
            'parent':'Kpneumo_r10.4_SUP_simplex', 'plexed':False, 'run': 2},
        'Kpneu_r10.4_SUP_duplex':{'flowcell': 'r10.4','model':'DUPLEX', 'species':'K.pneumoniae',
                'parent':'Kpneumo_r10.4_SUP_simplex', 'plexed':False, 'run': 2},
        'Kpneu_r10.4_SUP_SIM_3':{'flowcell': 'r10.4','model':'SUP', 'species':'K.pneumoniae',
            'parent':'Kpneumo_r10.4_SUP_simplex', 'plexed':False, 'run': 2},
        'Kpneu_r10.4_SUP_DUP_3':{'flowcell': 'r10.4','model':'DUPLEX', 'species':'K.pneumoniae',
                'parent':'Kpneumo_r10.4_SUP_simplex', 'plexed':False, 'run': 2},
        'MRSA_r10':{'flowcell': 'r10.3','model':'Rerio', 'species':'S.aureus',
                'parent':'MRSA_r10', 'plexed':False, 'run': 0},
        'MRSA_r10.3_SUP':{'flowcell': 'r10.3','model':'SUP', 'species':'S.aureus',
                'parent':'MRSA_r10', 'plexed':False, 'run': 0},
        'MRSA_r9':{'flowcell': 'r9.4.1','model':'HAC', 'species':'S.aureus',
                'parent':'MRSA_r9', 'plexed':True, 'run': 0},
        'MRSA_r9_sup':{'flowcell': 'r9.4.1','model':'SUP', 'species':'S.aureus',
                'parent':'MRSA_r9', 'plexed':True, 'run': 0},
        'MRSA_r10.4':{'flowcell': 'r10.4','model':'HAC', 'species':'S.aureus',
                'parent':'MRSA_r10.4', 'plexed':False, 'run': 0},
        'MRSA_r10.4_sup':{'flowcell': 'r10.4','model':'SUP', 'species':'S.aureus',
                'parent':'MRSA_r10.4', 'plexed':False, 'run': 0},
        'MRSA_r10.4_sup_dup':{'flowcell': 'r10.4','model':'DUPLEX', 'species':'S.aureus',
                'parent':'MRSA_r10.4', 'plexed':False, 'run': 0},
        'MRSA_r10.4_plex':{'flowcell': 'r10.4','model':'HAC', 'species':'S.aureus',
                'parent':'MRSA_r10.4_plex', 'plexed':True, 'run': 1},
        'MRSA_r10.4_plex_sup_dup':{'flowcell': 'r10.4','model':'DUPLEX', 'species':'S.aureus',
                'parent':'MRSA_r10.4_plex', 'plexed':True, 'run': 1},
        'MRSA_r10.4_plex_sup_dup_hybrid':{'flowcell': 'r10.4','model':'DUPLEX+SIMPLEX', 'species':'S.aureus',
                'parent':'MRSA_r10.4_plex', 'plexed':True, 'run': 1},
        'MRSA_r10.4_plex_sup_simp':{'flowcell': 'r10.4','model':'SUP', 'species':'S.aureus',
                'parent':'MRSA_r10.4_plex', 'plexed':True, 'run': 1},
        'MRSA_r10.4_SUP_simplex':{'flowcell': 'r10.4','model':'SUP', 'species':'S.aureus',
                'parent':'MRSA_r10.4_SUP_simplex', 'plexed':False, 'run': 2},
        'MRSA_r10.4_SUP_duplex':{'flowcell': 'r10.4','model':'DUPLEX', 'species':'S.aureus',
                'parent':'MRSA_r10.4_SUP_simplex', 'plexed':False, 'run': 2},
        'MRSA_r10.4_SUP_SIM_3':{'flowcell': 'r10.4','model':'SUP', 'species':'S.aureus',
                'parent':'MRSA_r10.4_SUP_simplex', 'plexed':False, 'run': 2},
        'MRSA_r10.4_SUP_DUP_3':{'flowcell': 'r10.4','model':'DUPLEX', 'species':'S.aureus',
                'parent':'MRSA_r10.4_SUP_simplex', 'plexed':False, 'run': 2},
        'Pa01_r10':{'flowcell': 'r10.3','model':'Rerio', 'species':'P.aeruginosa',
                'parent':'Pa01_r10', 'plexed':False, 'run': 0},
        'Pa01_r10.3_SUP':{'flowcell': 'r10.3','model':'SUP', 'species':'P.aeruginosa',
                'parent':'Pa01_r10', 'plexed':False, 'run': 0},
        'Pa01_r9':{'flowcell': 'r9.4.1','model':'HAC', 'species':'P.aeruginosa',
                'parent':'Pa01_r9', 'plexed':True, 'run': 0},
        'Pa01_r9_sup':{'flowcell': 'r9.4.1','model':'SUP', 'species':'P.aeruginosa',
                'parent':'Pa01_r9', 'plexed':True, 'run': 0},
        'Pa01_r10.4':{'flowcell': 'r10.4','model':'HAC', 'species':'P.aeruginosa',
                'parent':'Pa01_r10.4', 'plexed':False, 'run': 0},
        'Pa01_r10.4_sup':{'flowcell': 'r10.4','model':'SUP', 'species':'P.aeruginosa',
                'parent':'Pa01_r10.4', 'plexed':False, 'run': 0},
        'Pa01_r10.4_sup_dup':{'flowcell': 'r10.4','model':'DUPLEX', 'species':'P.aeruginosa',
                'parent':'Pa01_r10.4', 'plexed':False, 'run': 0},
        'Pa01_r10.4_plex':{'flowcell': 'r10.4','model':'HAC', 'species':'P.aeruginosa',
                'parent':'Pa01_r10.4_plex', 'plexed':True, 'run': 1},
        'Pa01_r10.4_plex_sup_dup':{'flowcell': 'r10.4','model':'DUPLEX', 'species':'P.aeruginosa',
                'parent':'Pa01_r10.4_plex', 'plexed':True, 'run': 1},
        'Pa01_r10.4_plex_sup_dup_hybrid':{'flowcell': 'r10.4','model':'DUPLEX+SIMPLEX', 'species':'P.aeruginosa',
                'parent':'Pa01_r10.4_plex', 'plexed':True, 'run': 1},
        'Pa01_r10.4_plex_sup_simp':{'flowcell': 'r10.4','model':'SUP', 'species':'P.aeruginosa',
                'parent':'Pa01_r10.4_plex', 'plexed':True, 'run': 1},
        'Pa01_r10.4_SUP_simplex':{'flowcell': 'r10.4','model':'SUP', 'species':'P.aeruginosa',
                'parent':'Pa01_r10.4_SUP_simplex', 'plexed':False, 'run': 2},
        'Pa01_r10.4_SUP_duplex':{'flowcell': 'r10.4','model':'DUPLEX', 'species':'P.aeruginosa',
                'parent':'Pa01_r10.4_SUP_simplex', 'plexed':False, 'run': 2},
        'Pa01_r10.4_SUP_SIM_3':{'flowcell': 'r10.4','model':'SUP', 'species':'P.aeruginosa',
                'parent':'Pa01_r10.4_SUP_simplex', 'plexed':False, 'run': 2},
        'Pa01_r10.4_SUP_DUP_3':{'flowcell': 'r10.4','model':'DUPLEX', 'species':'P.aeruginosa',
                'parent':'Pa01_r10.4_SUP_simplex', 'plexed':False, 'run': 2},
        'Ecoli_r1041_duplex':{'flowcell': 'r10.4.1','model':'DUPLEX', 'species':'E.coli',
                'parent':'Ecoli_r1041_simplex', 'plexed':False, 'run': 3},
        'Ecoli_r1041_simplex':{'flowcell': 'r10.4.1','model':'SUP', 'species':'E.coli',
                'parent':'Ecoli_r1041_simplex', 'plexed':False, 'run': 3},
        'Pa01_BSA_r1041_duplex':{'flowcell': 'r10.4.1','model':'DUPLEX', 'species':'P.aeruginosa',
                'parent':'Pa01_BSA_r1041_simplex', 'plexed':False, 'run': 3},
        'Pa01_BSA_r1041_simplex':{'flowcell': 'r10.4.1','model':'DUPLEX', 'species':'P.aeruginosa',
                'parent':'Pa01_BSA_r1041_simplex', 'plexed':False, 'run': 3},
        'Kpneu_BSA_r1041_duplex':{'flowcell': 'r10.4.1','model':'DUPLEX', 'species':'K.pneumoniae',
            'parent':'Kpneu_BSA_r1041_simplex', 'plexed':False, 'run': 3},
        'Kpneu_BSA_r1041_simplex':{'flowcell': 'r10.4.1','model':'SUP', 'species':'K.pneumoniae',
            'parent':'Kpneu_BSA_r1041_simplex', 'plexed':False, 'run': 3},
        'MRSA_BSA_r1041_duplex':{'flowcell': 'r10.4.1','model':'DUPLEX', 'species':'S.aureus',
                'parent':'MRSA_BSA_r1041_simplex', 'plexed':False, 'run': 3},
        'MRSA_BSA_r1041_simplex':{'flowcell': 'r10.4.1','model':'SUP', 'species':'S.aureus',
                'parent':'MRSA_BSA_r1041_simplex', 'plexed':False, 'run': 3}
        }

subSampleDict={}
for i in sampleDict:
    sampleDict[i]['subSample']=0
    for subSample in [10, 20, 30, 40, 50, 100]:
        subSampleDict['{0}_{1}'.format(i,subSample)]=sampleDict[i].copy()
        subSampleDict['{0}_{1}'.format(i,subSample)]['subSample']=subSample

sampleDict=sampleDict|subSampleDict

sampleDictDF=pd.DataFrame(sampleDict).T
sampleDictDF.to_csv('sample_meta.csv')



sampleAssemblyDict={}
for assembler in ['flye','flye_hq', 'flye_medaka',
        'flye_medakaX2', 'flye_medakaX3',
        'canu','unicycler','spades']:
    for i in sampleDict:
        sampleAssemblyDict['{0}_{1}'.format(i,assembler)]=sampleDict[i].copy()
        sampleAssemblyDict['{0}_{1}'.format(i,assembler)]['Assembler']=assembler

sampleAssemblyDictDF=pd.DataFrame(sampleAssemblyDict).T
sampleAssemblyDictDF.to_csv('sample_assembly_meta.csv')

def getSampleDetails(r):
    d=sampleDict[r['name']]
    r['Flowcell']=d['flowcell']
    r['Model']=d['model']
    r['FC+Model']=d['flowcell'] + ' ' + d['model']
    r['Species']=d['species']
    r['parent']=d['parent']
    r['plexed']=d['plexed']
    r['run']=d['run']
    if 'subSample' in d:
        r['subSample'] = d['subSample']
    else:
        r['subSample'] = 0

    #if '_plex_' in r['name']:
    #    r['plexed']=True
    #else:
    #    r['plexed']=False
    return r

def getAssemblyDetails(r):
    d=sampleAssemblyDict[r['Assembly']]
    r['Flowcell']=d['flowcell']
    r['Model']=d['model']
    r['FC+Model']=d['flowcell'] + ' ' + d['model']
    r['Species']=d['species']
    r['Assembler']=d['Assembler']
    r['parent']=d['parent']
    r['run']=d['run']
    r['plexed']=d['plexed']
    if d['Assembler']=='spades':
        r['Flowcell']='Illumina'
        r['Model']='Illumina'
        r['FC+Model']='Illumina'
    if d['Assembler']=='unicycler':
        r['Flowcell']='r9.4.1+Illumina'
        r['Model']='r9.4.1+Illumina'
        r['FC+Model']='r9.4.1+Illumina'
    if 'subSample' in d:
        r['subSample'] = d['subSample']
    else:
        r['subSample'] = 0

    #if 'plex' in r['Assembly']:
    #    r['plexed']=True
    #else:
    #    r['plexed']=False
    return r

def filterSUPspades(r):
    if r['Assembler'] in ['spades','unicycler']:
        if 'sup' in r['Assembly']:
            r['SUP filter']=True
        else:
            r['SUP filter']=False
    else:
        r['SUP filter']=False
    return r

headers=['qseqid',
        'sseqid', 
        'pident',
        'length',
        'mismatch',
        'gapopen',
        'qstart',
        'qend',
        'sstart',
        'send',
        'evalue',
        'bitscore']

refs=['Ecoli',
        'Kpneumo',
        'MRSA252',
        'Pa01']

bugDict={'ecoli': 'E.coli',
            'kpneumo':'K.pneumoniae',
            'Kpneumo':'K.pneumoniae',
            'Kpneu':'K.pneumoniae',
            'Ecoli':  'E.coli',
            'Pa01':   'P.aeruginosa',
            'MRSA':   'S.aureus'
            }

flowcells=['r9','r10','r10.4','q20','r10.4.1']

flowcellDict={'r10':'r10.3',
        'r10.3_SUP':'r10.3_sup',
        'r9':'r9.4',
        'r9_sup':'r9.4_sup',
        'r10.4':'r10.4',
        'r10.4_sup':'r10.4_sup',
        'r10.4_sup_dup':'r10.4_sup_dup',
        'r10.4_plex':'r10.4_plex',
        'r10.4_plex_sup_dup':'r10.4_plex_sup_dup',
        'r10.4_plex_sup_dup_hybrid':'r10.4_plex_sup_dup_hybrid',
        'r10.4_plex_sup_simp':'r10.4_plex_sup_simp',
        'BSA_r1041_simplex':'r10.4.1_sup',
        'BSA_r1041_duplex':'r10.4.1_dup',
        'r1041_simplex':'r10.4.1_sup',
        'r1041_duplex':'r10.4.1_dup',
        'Reference':'Reference'}

subs=[2, 5, 10, 20, 50, 100]

def getNameFlow(r):
    suff=r['Sample name'].split('_')
    if len(suff)>2:
        r['Flow cell']=flowcellDict['_'.join(suff[1:])]
    else:
        r['Flow cell']=flowcellDict[suff[-1]]
    r['Sample name']=bugDict[r['Sample name'].split('_')[0]]
    return r


def get_assembly_stats():
    assemblers=['unicycler','flye', 'canu','spades']
    dfs=[]
    for sample in sampleAssemblyDict:
        try:
            df=pd.read_csv('CSVs/standard_seqkitContigs/{0}.tsv'.format(sample),
                            sep='\t')
            #df['Bug']=sample.split('_')[0]
            df['Sample name']=sample
            df['Assembly']=sample
            df=df.apply(getAssemblyDetails,axis=1)
            #df['Assembler']=assembler
            dfs.append(df)
        except:
            pass
    for ref in ['ecoli',
        'kpneumo',
        'MRSA',
        'Pa01']:
        df=pd.read_csv('CSVs/standard_seqkitContigsRef/{0}_reference.tsv'.format(ref),
                        sep='\t')
        df['Species']=bugDict[ref]
        df['Flowcell']='Reference'
        df['plexed']=False
        df['Model']='Reference'
        df['FC+Model']='Reference'
        df['Sample name']=ref
        df['Assembler']='Reference'
        df['subSample']=0
        dfs.append(df)

    df=pd.concat(dfs)
    df=df.reset_index()
    df.loc[df['Assembler'] == 'spades', 'Flowcell'] = 'Illumina'
    df.drop_duplicates(subset=['Assembly','Assembler','Species','Flowcell'],inplace=True)
    #df['Species']=df['Bug'].map(bugDict)
    print(df)
    df.to_csv('CSVs/Assembly_basic_stats.csv')
    df['Contigs']=df['num_seqs']
    df['Longest Contig']=df['max_len']
    df['Flowcell']=df['Flowcell'].map(flowcellDict)
    df2=df[df['subSample']==0]
    #df2=df2[df2['plexed']==False]
    #print(df2)
    #df2=df2.pivot(index='Species', columns=['Flowcell','Model','Assembler'], 
    #        values=['Contigs','Longest Contig','N50'])
    #print(df2)
    #df2.to_csv('CSVs/Assembly_basic_stats_perBug.csv')
    #df2.to_excel('CSVs/Assembly_basic_stats_perBug.xlsx')


    # plots
    hue_order=['r9.4.1 HAC', 'r9.4.1 SUP',
            'r10.3 Rerio','r10.3 SUP',
            'r10.4 HAC', 'r10.4 SUP','r10.4 DUPLEX',
            'r9.4.1+Illumina', 'Illumina', 'Reference']
    df=df[df['plexed']==False]
    df=df[df['Assembler']!='canu']
    ax1=sns.catplot(x='subSample', y='max_len', data=df, 
            hue='FC+Model',
            kind='bar',
            col='Species',
            hue_order=hue_order)
    plt.savefig('figs/Assembly_Subsample_maxlen.png')
    plt.savefig('figs/Assembly_Subsample_maxlen.pdf')
    plt.savefig('figs/Assembly_Subsample_maxlen.svg')
    plt.show()


def assembly_stats(assembler):
    dfs=[]
    for sample in samples:
        try:
            df=pd.read_csv('blastn/{1}/{0}.blast.tsv'.format(sample,assembler),
                sep='\t',
                names=headers)
        except:
            continue
        df=df[df['length']>100000]
        df['sample']=sample
        df['assembler']=assembler
        dfs.append(df)
    
    
    df=pd.concat(dfs)
    df['Sample name']=df['sample']
    df=df.apply(getNameFlow, axis=1)
    df.loc[df['assembler'] == 'spades', 'Flow cell'] = 'Illumina'
    df['gaps per 100kb']=(df['gapopen']/df['length'])*100000
    df['Mismatches per 100kb']=(df['mismatch']/df['length'])*100000
    g=df.groupby(['Sample name','Flow cell'])[['length','mismatch','gapopen']].sum()
    g=g.reset_index()
    g['% mismatch']=100*(g['mismatch']/g['length'])
    g['% gaps']=100*(g['gapopen']/g['length'])
    g['gaps per 100kb']=(g['gapopen']/g['length'])*100000
    g['assembler']=assembler
    print(g)
    g.to_csv('CSVs/{0}_assembly_blastn_details.csv'.format(assembler))

    return df


def get_assembly_DNAdiff_stats():
    df=pd.read_csv('CSVs/dnadiff.csv')
    df['Assembly']=df['Sample_name'] + '_' + df['Assembler']
    df=df.apply(getAssemblyDetails,axis=1)
    df['Number']=df['Query']
    df=df.apply(filterSUPspades,axis=1)
    df=df[df['SUP filter']==False]
    ## plots
    hue_order=['r9.4.1 HAC', 'r9.4.1 SUP',
            'r10.3 Rerio','r10.3 SUP',
            'r10.4 HAC', 'r10.4 SUP','r10.4 DUPLEX',
            'r10.4.1 SUP','r10.4.1 DUPLEX',
            'r9.4.1+Illumina', 'Illumina']

    df2=df[df['run']==0]
    df2=df2[df2['subSample']==0]
    df2=df2[df2['plexed']==False]
    print(df2)
 
    ax1=sns.catplot(x='Assembler', y='Number', data=df2, hue='FC+Model',
            kind='bar',
            col='Species',row='type',
            hue_order=hue_order, log=True)
    plt.savefig('figs/Assembly_dnadiff.png')
    plt.savefig('figs/Assembly_dnadiff.pdf')
    plt.savefig('figs/Assembly_dnadiff.svg')
    plt.show()
    plt.clf()

    # per subsample
    hue_order=['r9.4.1 HAC', 'r9.4.1 SUP',
            'r10.3 Rerio','r10.3 SUP',
            'r10.4 HAC', 'r10.4 SUP','r10.4 DUPLEX',
            'r10.4.1 SUP','r10.4.1 DUPLEX',
            'r9.4.1+Illumina']
    df3=df[df['plexed']==False]
    df3=df3[df3['Assembler'].isin(['flye_hq','unicycler'])]
    ax1=sns.catplot(x='subSample', y='Number', data=df3, hue='FC+Model',
            kind='bar',
            col='Species',row='type',
            hue_order=hue_order, log=True)
    plt.savefig('figs/Assembly_dnadiff_subsample.png')
    plt.savefig('figs/Assembly_dnadiff_subsample.pdf')
    plt.savefig('figs/Assembly_dnadiff_subsample.svg')
    plt.show()
    plt.clf()

def get_assembly_QUAST_stats():
    dfs,dfs2=[],[]
    for bug in ['ecoli','kpneumo','MRSA','Pa01']:
        df=pd.read_csv('quast_IC/standard_quast/{0}_output/contigs_reports/transposed_report_misassemblies.tsv'.format(bug),  sep='\t')
        dfs.append(df)
        df=pd.read_csv('quast_IC/standard_quast/{0}_output/transposed_report.tsv'.format(bug),  sep='\t')
        dfs2.append(df)
    df=pd.concat(dfs)
    df2=pd.concat(dfs2)
    df=df.apply(getAssemblyDetails,axis=1)
    df=df.merge(df2, on='Assembly',how='left')
    df['Mismatches per 100kb']=(df['# mismatches']/df['Reference length'])*100000
    df['INDELs per 100kb']=(df['Indels length']/df['Reference length'])*100000
    df=df.apply(filterSUPspades,axis=1)
    print(df)
    df.to_csv('CSVs/assembly_quast_details.csv',index=False)

    ## plots
    hue_order=['r9.4.1 HAC', 'r9.4.1 SUP',
            'r10.3 Rerio','r10.3 SUP',
            'r10.4 HAC', 'r10.4 SUP','r10.4 DUPLEX',
            'r10.4 DUPLEX+SIMPLEX',
            'r9.4.1+Illumina', 'Illumina']

    df2=df[df['subSample']==0]
    df2=df2[df2['plexed']==False]
    df2=df2[df2['SUP filter']==False]


    ax1=sns.catplot(x='Assembler', y='# mismatches per 100 kbp', data=df2, hue='FC+Model',
            kind='bar',
            col='Species', hue_order=hue_order)
    plt.savefig('figs/Assembly_nucmerMismatches100k.png')
    plt.savefig('figs/Assembly_nucmerMismatches100k.pdf')
    plt.savefig('figs/Assembly_nucmerMismatches100k.svg')
    plt.show()
    plt.clf()
    
    ax2=sns.catplot(x='Assembler', y='# indels per 100 kbp', data=df2, hue='FC+Model',
            kind='bar',
            col='Species', hue_order=hue_order)
    plt.savefig('figs/Assembly_nucmerINDELs.png')
    plt.savefig('figs/Assembly_nucmerINDELs.pdf')
    plt.savefig('figs/Assembly_nucmerINDELs.svg')
    plt.show()
    plt.clf()

def get_assembly_Blast_stats():
    assemblers=['unicycler','flye','spades','canu']
    dfs=[]
    for assembler in assemblers:
        dfs.append(assembly_stats(assembler))
    df=pd.concat(dfs)
    df.to_csv('CSVs/assembly_blastn_details.csv',index=False)
    g=df.groupby(['Sample name','Flow cell','assembler'])[['pident','mismatch','gapopen','gaps per 100kb', 'Mismatches per 100kb']].describe()
    g.to_csv('CSVs/assembly_blastn_details_summary.csv')


    ## plots
    hue_order=['r9.4', 'r9.4_sup',
            'r10.3','r10.3_sup',
            'r10.4', 'r10.4_sup','r10.4_sup_dup',
            'r10.4_plex', 'r10.4_plex_sup_simp','r10.4_plex_sup_dup', 'r10.4_plex_sup_dup_hybrid',
            'Illumina']
    #hue_order=['r9.4', 'r10.3', 'r10.4','r10.4_sup','r10.4_sup_dup', 'Illumina']
    #ax1=sns.swarmplot(x='sample',y='gaps per 100kb',data=df, hue='assembler')
    #ax1=sns.boxplot(y='sample',x='gaps per 100kb',data=df, hue='assembler')
    ax1=sns.catplot(y='assembler',x='gaps per 100kb',data=df, hue='Flow cell',
            hue_order=hue_order,
            col='Sample name',
            kind='box',
            height=4,col_wrap=2)#, aspect=.7)
    #plt.tight_layout()
    plt.xlim(0,40)
    #ax1.fig.get_axes()[0].set_xscale('log')
    plt.savefig('figs/Assembly_gaps100k.png')
    plt.savefig('figs/Assembly_gaps100k.pdf')
    plt.savefig('figs/Assembly_gaps100k.svg')
    plt.show()
    plt.clf()

    ## mismatches
    #ax1=sns.boxplot(y='sample',x='Mismatches per 100kb',data=df, hue='assembler')
    ax1=sns.catplot(y='assembler',x='Mismatches per 100kb',data=df, hue='Flow cell',
            hue_order=hue_order,
            col='Sample name',
            kind='box',
            height=4,col_wrap=2)#, aspect=.7)
    plt.tight_layout()
    #plt.set_xscale('log')
    plt.xlim(0,30)
    plt.savefig('figs/Assembly_Mismatches100k.png')
    plt.savefig('figs/Assembly_Mismatches100k.pdf')
    plt.savefig('figs/Assembly_Mismatches100k.svg')
    plt.show()
    plt.clf()

def raw_read_stats():
    dfs=[]
    for sample in samples:
        df=pd.read_csv(gzip.open('mapcsvs/{0}.read_stats.csv.gz'.format(sample)))
        df=df.sample(1000)
        df=df.apply(getNameFlow, axis=1)
        dfs.append(df)
    for sample in ['ecoli', 'MRSA', 'Pa01', 'kpneumo']:
        df=pd.read_csv(gzip.open('mapcsvs/{0}.read_stats.csv.gz'.format(sample)))
        df=df.sample(1000)
        df['Sample name']=sample
        df['Flow cell']='Illumina'
        dfs.append(df)
    
    df=pd.concat(dfs)
    df['Sample name']=df['Sample name'].replace(bugDict)
    df['Probability of incorrect base call']=df['NM']/df['query_length']
    df['log']=np.log10(df['Probability of incorrect base call'])
    df['PHRED score']=-10*df['log']
    df['PHRED score non-indels']=-10*np.log10(df['Subs']/df['query_length'])
    df.to_csv('CSVs/raw_read_stats.csv',index=False)
   
    # plots
    #hue_order=['r9.4', 'r10.3', 'r10.4', 'r10.4_sup', 'r10.4_sup_dup','Illumina']
    hue_order=['r9.4', 'r9.4_sup',
            'r10.3','r10.3_sup',
            'r10.4', 'r10.4_sup','r10.4_sup_dup',
            'r10.4.1_sup', 'r10.4.1_dup',
            'r10.4_plex', 'r10.4_plex_sup_simp','r10.4_plex_sup_dup', 'r10.4_plex_sup_dup_hybrid',
            'Illumina']
    ax1=sns.boxplot(x='Sample name', y='% match', 
            hue='Flow cell',hue_order=hue_order, 
            data=df)
    #ax1.set( yscale="log")
    plt.ylim(90,101)
    plt.savefig('figs/Percent_match.png')
    plt.savefig('figs/Percent_match.pdf')
    plt.savefig('figs/Percent_match.svg')
    #plt.show()
    plt.clf()
    
    ax3=sns.boxplot(x='Sample name', y='PHRED score',
            hue='Flow cell',hue_order=hue_order,
            data=df)
    #ax1.set( yscale="log")
    plt.savefig('figs/PHRED.png')
    plt.savefig('figs/PHRED.pdf')
    plt.clf()
    
    ax4=sns.boxplot(x='Sample name', y='PHRED score non-indels',
            hue='Flow cell', hue_order=hue_order,
            data=df)
    #ax1.set( yscale="log")
    plt.savefig('figs/PHRED_subs_only.pdf')
    plt.savefig('figs/PHRED_subs_only.png')
    plt.clf()
    
    #ax2=sns.boxplot(x='Sample name', y='% ins', hue='Flow cell',data=df)
    ##ax1.set( yscale="log")
    #plt.show()

def getFlow(r):
    try:
        f=r.split('_')[-1]
    except:
        f=None
    return f

def getBug(r):
    return r.split('_')[0]

def variant_callingSNPs():
    ref_muts=[]
    for ref in refs:
        df=pd.read_csv('muts/{}_muts.csv'.format(ref))
        df['ref_name']=ref
        df['POS']=df['pos']+1
        df['CHROM']=df['Chrom']
        ref_muts.append(df)

    #predicted muts
    pm=pd.concat(ref_muts)

    vcf_cols=['CHROM',
            'POS',
            'ID',
            'REF',
            'ALT',
            'QUAL',
            'FILTER',
            'INFO',
            'FORMAT', 
            'SAMPLE']
    vcfs=[]
    filteredVcfs=[]
    for sample in samples:
        for sub in subs:
            df=pd.read_csv('vcfs/{0}_{1}.vcf'.format(sample, sub),
                    comment='#',
                    sep='\t',
                    names=vcf_cols)
            df['sample']=sample
            df['sub']=sub
            vcfs.append(df)
            ## load filtered VCFs
            try:
                fc=sample.split('_')[-1]
                df=pd.read_csv('forest_filter/vcfs/mutated_filterSNPs/{0}_{1}_Medaka_combined_{2}.vcf'.format(sample, sub, fc),
                        comment='#',
                        sep='\t',
                        names=vcf_cols)
                df['sample']=sample
                df['sub']=sub
                filteredVcfs.append(df)
            except:
                pass

    for sample in ['MRSA','Pa01','ecoli','kpneumo']:
        df=pd.read_csv('vcfs/mutated_snippy/{0}_output/snps.vcf'.format(sample),
                        comment='#',
                        sep='\t',
                        names=vcf_cols)
        df['sample']='{0}_Illumina'.format(sample)
        df['sub']=100
        vcfs.append(df)
        filteredVcfs.append(df)

    muts=pd.concat(vcfs)
    filterMuts=pd.concat(filteredVcfs)
    snpStats(pm, muts, 'unfiltered')
    snpStats(pm, filterMuts, 'filtered')


def snpStats(pm, muts, condition):
    # Merge left generated muts with right variant calls - For sensitivity
    df=pm.merge(muts, on=['CHROM','POS'], how='left')
    df['Flow cell']=df['sample'].map(getFlow)
    df.fillna('FAIL',inplace=True)

    # groupby - need to do something to show what % of SNPs/INDELs are found
    g=df.groupby(['CHROM','sample','Flow cell','sub'])['POS'].count()
    df.to_csv('CSVS/SNPs_expected_found_count_{0}.csv'.format(condition))
    g=g.reset_index()
    g.rename(columns={'POS':'TP'},inplace=True)
    g['Sensitivity']=(g['TP']/1000)*100
    sensitivity=g[g['sample']!='FAIL']
    sensitivity.to_csv('CSVs/SNPs_expected_found_count_{0}.csv'.format(condition))

    # Merge left variant calls with right generated muts - For specificity
    muts=muts.merge(pm, on=['CHROM','POS'], how='left')
    muts['Flow cell']=muts['sample'].map(getFlow)
    muts['Reference']=muts['sample'].map(getBug)
    muts['Reference']=muts['Reference'].replace(bugDict)
    muts.fillna('FP',inplace=True)
    muts['SNP type']=np.where(muts['ref_name']=='FP','FP','TP')
    muts.to_csv('CSVs/SNPs_found_expected_{0}.csv'.format(condition))
   
    g=muts.groupby(['CHROM','sample','sub','ref_name'])['POS'].count()

    sensSpec=g.reset_index()
    sensSpec.rename(columns={'POS':'Positions'},inplace=True)
    sensSpec.to_csv('CSVs/SNPs_found_expected_count_{0}.csv'.format(condition))

    ## PLOTS
    ax1=sns.boxplot(x='sub', y='QUAL',hue='SNP type', data=muts)
    #ax1=sns.swarmplot(x='sub', y='QUAL',hue='SNP type', data=muts)
    plt.ylim(0,100)
    plt.savefig('figs/QUAL_boxplot_{0}.png'.format(condition))
    plt.savefig('figs/QUAL_boxplot_{0}.pdf'.format(condition))
    #plt.show()
    plt.clf()

    ax2=sns.catplot(x='sub', y='QUAL', hue='SNP type', data=muts,
            col='Flow cell', row='Reference',
            kind="box")
    plt.ylim(0,100)
    plt.savefig('figs/QUAL_boxplot_cat_{0}.png'.format(condition))
    plt.savefig('figs/QUAL_boxplot_cat_{0}.pdf'.format(condition))
    #plt.show()
    plt.clf()


    muts['altSize']=muts['ALT'].map(len)
    muts['refSize']=muts['REF'].map(len)
    fdf=muts#[muts['QUAL']>=20]
    fdf=fdf[fdf['altSize']==1]
    fdf=fdf[fdf['refSize']==1]

    g=fdf.groupby(['CHROM','sample','sub','Flow cell','Reference','SNP type'])['POS'].count()
    g=g.reset_index()
    ax3=sns.catplot(x='sub', y='POS',
            hue='SNP type', data=g, 
            col='Flow cell', row='Reference',
            kind='bar')
    #plt.ylim(0,2000)
    plt.savefig('figs/TP_FP_barplot_cat_{0}.png'.format(condition))
    plt.savefig('figs/TP_FP_boxplot_cat_{0}.pdf'.format(condition))
    #plt.show()

def variant_callingINDELs():
    # Load generated mutation positions
    ref_muts=[]
    refs=['Ecoli',
        'Kpneumo',
        'MRSA',
        'Pa01']

    for ref in refs:
        df=pd.read_csv('refs/INDELs/{}_muts.csv'.format(ref))
        df['ref_name']=ref
        df['POS']=df['pos']#+1
        df['CHROM']=df['Chrom']
        ref_muts.append(df)
    #predicted muts
    pm=pd.concat(ref_muts)

    # load variant calls from VCF files
    vcf_cols=['CHROM',
            'POS',
            'ID',
            'REF',
            'ALT',
            'QUAL',
            'FILTER',
            'INFO',
            'FORMAT', 
            'SAMPLE']
    vcfs=[]
    filteredVcfs=[]
    for sample in samples:
        for sub in subs:
            df=pd.read_csv('forest_filter/vcfs/indels_medaka/{0}_{1}.vcf'.format(sample, sub),
                        comment='#',
                        sep='\t',
                        names=vcf_cols)
            df['sample']=sample
            df['sub']=sub
            vcfs.append(df)
            ## load filtered VCFs
            fc=sample.split('_')[-1]
            df=pd.read_csv('forest_filter/vcfs/indels_filterSNPs/{0}_{1}_Medaka_combined_{2}.vcf'.format(sample, sub, fc),
                        comment='#',
                        sep='\t',
                        names=vcf_cols)
            df['sample']=sample
            df['sub']=sub
            filteredVcfs.append(df)



    for sample in ['MRSA','Pa01','ecoli','kpneumo']:
        df=pd.read_csv('vcfs/indels_snippy/{0}_output/snps.vcf'.format(sample),
                        comment='#',
                        sep='\t',
                        names=vcf_cols)
        df['sample']='{0}_Illumina'.format(sample)
        df['sub']=100
        vcfs.append(df)
        filteredVcfs.append(df)


    muts=pd.concat(vcfs)
    filteredMuts=pd.concat(filteredVcfs)
    indelStats(pm,muts,'unfiltered')
    indelStats(pm,filteredMuts,'filtered')

def indelStats(pm, muts, condition):
    # Merge left generated muts with right variant calls - For sensitivity
    df=pm.merge(muts, on=['CHROM','POS'], how='left')
    df['Flow cell']=df['sample'].map(getFlow)
    df.fillna('FAIL',inplace=True)

    # groupby - need to do something to show what % of SNPs/INDELs are found
    g=df.groupby(['CHROM','sample','Flow cell','sub'])['POS'].count()
    df.to_csv('CSVs/INDEL_expected_found_{0}.csv'.format(condition))
    g=g.reset_index()
    g.rename(columns={'POS':'TP'},inplace=True)
    g['Sensitivity']=(g['TP']/1000)*100
    sensitivity=g[g['sample']!='FAIL']
    sensitivity.to_csv('CSVs/INDEL_expected_found_count_{0}.csv'.format(condition))

    # Merge left variant calls with right generated muts - For specificity
    muts=muts.merge(pm, on=['CHROM','POS'], how='left')
    muts['Flow cell']=muts['sample'].map(getFlow)
    muts['Reference']=muts['sample'].map(getBug)
    muts['Reference']=muts['Reference'].replace(bugDict)
    muts.fillna('FP',inplace=True)
    muts['SNP type']=np.where(muts['ref_name']=='FP','FP','TP')
    muts['ALT_len']=muts['ALT'].map(len)
    muts['REF_len']=muts['REF'].map(len)
    muts['INDEL length']=muts['ALT_len'] -  muts['REF_len']
    muts['INDEL length']=muts['INDEL length'].map(abs)
    muts.to_csv('CSVs/INDEL_found_expected_{0}.csv'.format(condition))
    
    g=muts.groupby(['CHROM','sample','Flow cell','sub','Reference','SNP type'])['POS'].count()
    sensSpec=g.reset_index()
    sensSpec.rename(columns={'POS':'Positions'},inplace=True)
    sensSpec.to_csv('CSVs/INDEL_found_expected_count_{0}.csv'.format(condition))

    ## PLOTS

    # box qual plots
    ax1=sns.boxplot(x='sub', y='QUAL',hue='SNP type', data=muts)
    #ax1=sns.swarmplot(x='sub', y='QUAL',hue='SNP type', data=muts)
    plt.ylim(0,500)
    plt.savefig('figs/QUAL_boxplot_INDELs_{0}.png'.format(condition))
    plt.savefig('figs/QUAL_boxplot_INDELs_{0}.pdf'.format(condition))
    #plt.show()
    plt.clf()
    plt.clf()
    plt.cla()
    plt.close()


    ## catplot box plots qual by flow cell and species
    ax2=sns.catplot(x='sub', y='QUAL', hue='SNP type', data=muts,
            col='Flow cell', row='Reference',
            kind="box")
    plt.ylim(0,500)
    plt.savefig('figs/QUAL_boxplot_cat_INDELs_{0}.png'.format(condition))
    plt.savefig('figs/QUAL_boxplot_cat_INDELs_{0}.pdf'.format(condition))
    #plt.show()
    plt.clf()
    plt.cla()
    plt.close()
    plt.clf()

    # catplot bar plots of number of TPs or FPs by flow cell and species
    #g=fdf.groupby(['CHROM','sample','sub','Flow cell','Reference','SNP type'])['POS'].count()
    #g=g.reset_index()
    ax3=sns.catplot(x='sub', y='Positions',
            hue='SNP type', data=sensSpec, 
            col='Flow cell', row='Reference',
            kind='bar')
    plt.ylim(0,2000)
    plt.savefig('figs/TP_FP_barplot_cat_INDELs_{0}.png'.format(condition))
    plt.savefig('figs/TP_FP_boxplot_cat_INDELs_{0}.pdf'.format(condition))
    #plt.show()
    plt.cla()
    plt.close()
    plt.clf()

    # INDEL size by type
    g=muts.groupby(['CHROM','sample','Flow cell','sub','Reference','SNP type','INDEL length'])['POS'].count()
    g=g.reset_index()
    g.rename(columns={'POS':'Positions'},inplace=True)
    ax4=sns.boxplot(x='INDEL length', y='Positions', data=g, hue='SNP type')
    plt.ylim(0,100)
    plt.xlim(0,11)
    plt.savefig('figs/INDEL_length_TP_FP_{0}.png'.format(condition))
    plt.savefig('figs/INDEL_length_TP_FP_{0}.pdf'.format(condition))
    #plt.show()
    plt.clf()
    plt.cla()
    plt.close()

def raw_sequencing_stats():
    dfs=[]

    for sample in samples:
        df=pd.read_csv('CSVs/standard_seqkitNanopore/{0}.tsv'.format(sample),
            sep='\t',
            )
        df['Sample name']=sample
        df['Bug']=sample.split('_')[0]
        df=df.apply(getNameFlow,axis=1)
        dfs.append(df)

    illuminaSamples=['MRSA',
            'Pa01',
            'ecoli',
            'kpneumo']
    for sample in illuminaSamples:
        df=pd.read_csv('CSVs/standard_seqkitIllumina/{0}.tsv'.format(sample),
            sep='\t',
            )
        df['Sample name']=sample
        df['Bug']=sample.split('_')[0]
        df['Flow cell']='Illumina'
        df['sum_len']=df['sum_len']*2
        df['num_seqs']=df['num_seqs']*2

        dfs.append(df)

    df=pd.concat(dfs)
    df['Species']=df['Bug'].replace(bugDict)
    df['Total bases']=df['sum_len']
    df['Total bases (MB)']=df['Total bases']/1000000
    df2=df[['Species','Sample name','Flow cell', 'num_seqs','Total bases', 'min_len', 'avg_len','max_len', 'N50','Q20(%)']]
    df2.to_csv('CSVs/raw_sequencing_stats.csv',index=False)
    # plots
    hue_order=['r9.4', 'r9.4_sup',
            'r10.3','r10.3_sup',
            'r10.4', 'r10.4_sup','r10.4_sup_dup',
            'r10.4_plex', 'r10.4_plex_sup_simp','r10.4_plex_sup_dup', 'r10.4_plex_sup_dup_hybrid',
            'Illumina']
    ax1=sns.barplot(x='Total bases (MB)',y='Species',
            hue='Flow cell', hue_order=hue_order,
            data=df)
    #ax1.set(xscale="log")
    plt.tight_layout()
    plt.savefig('figs/Total_bases_bar.svg')
    plt.savefig('figs/Total_bases_bar.png')
    plt.savefig('figs/Total_bases_bar.pdf')
    plt.show()
    plt.clf()

def getQUASTdetails(r):
    r['Assembler']=r['Assembly'].split('_')[-1]
    r['Flowcell']=r['Assembly'].split('_')[1]
    r['Flowcell']=flowcellDict[r['Flowcell']]
    return r

def geneFeatures():
    bugs={'MRSA':'MRSA_output',
            'Pa01':'Pa01_output',
            'E.coli':'ecoli_output',
            'K.pneumo':'kpneumo_output'}
    dfs=[]
    for bug in bugs:
        df=pd.read_csv('quast/standard_quast/{0}/transposed_report.tsv'.format(bugs[bug]),
                sep='\t')
        df['Bug']=bug
        dfs.append(df)

    df=pd.concat(dfs)
    df=df.apply(getQUASTdetails,axis=1)
    print(df)
    
    # plots
    ax1=sns.catplot(x='Assembler', y='Genome fraction (%)', 
            hue='Flowcell', data=df,
            kind='bar', col='Bug',
            col_wrap=2)
    plt.ylim(96,100)
    plt.show()

def prokkaGenes():
    dfs=os.listdir('gbk_compare/standard_compareProkka/')
    dfs=[i for i in dfs if i.endswith('.csv')]
    l=[]
    for f in dfs:
        l.append(pd.read_csv('gbk_compare/standard_compareProkka/{}'.format(f)))
    df=pd.concat(l)
    
    # Filter out _N incremented genes
    def isIncrement(r):
        if '_' in r: return True
        else:        return False

    #df['incremented']=df['Gene name'].map(isIncrement)
    #df=df[df['incremented']==False]


    ##### get reference genes ######
    gbks={'AE014075.1.gbk':'E.coli',
            'CP000647.1.gbk':'K.pneumoniae',
            'NC_002952.2.gbk':'S.aureus',
            'NC_002516.2.gbk':'P.aeruginosa'
            }
    l=[]
    for gbk in gbks:
        for seq_record in SeqIO.parse('GBK/{0}'.format(gbk), "genbank"):
            for seq_feature in seq_record.features:
                if seq_feature.type == "CDS":
                    if 'gene' in seq_feature.qualifiers:
                        if 'translation' not in seq_feature.qualifiers: continue
                        d={'Species':gbks[gbk],
                                'Gene name':seq_feature.qualifiers['gene'][0],
                                'Gene AA length':len(seq_feature.qualifiers['translation'][0]),
                                'Notes':'Reference'}
                        l.append(d)

    rdf=pd.DataFrame(l)
    print(rdf)


    ###### reduce to % #######
    g=df.groupby(['Sample name','Notes'])[['Gene found','name match']].sum().reset_index()
    print(g)
    
    g1=df.groupby(['Sample name','Notes'])[['Ref Gene name']].count().reset_index()
    g=g.merge(g1,on=['Sample name','Notes'],how='left')
    g['Percentage of genes found']=(g['Gene found']/g['Ref Gene name'])*100
    g['Percentage of names matching']=(g['name match']/g['Ref Gene name'])*100
    
    g['Assembly']=g['Sample name'] + '_' + g['Notes']
    g.to_csv('CSVs/prokka_genes_assembly_raw.csv')
    g=g.apply(getAssemblyDetails,axis=1)

    print(g)
    g1=g[g['Assembler'].isin(['flye','flye_hq', 'flye_medaka','flye_medakaX2','flye_medakaX3'])]
    g1=g1[g1['run']==0]
    g1=g1[g1['plexed']==False]
    g2=g[g['Assembler'].isin(['unicycler']) & g['Flowcell'].isin(['r9.4.1+Illumina'])]
    g3=g[g['Assembler'].isin(['spades']) & g['Flowcell'].isin(['Illumina'])]
    g1=pd.concat([g1,g2,g3])
    g1.to_csv('CSVs/genes_found_flowcell.csv',index=False)
    g2=g1[g1['subSample']==0]
    
    hue_order=['r9.4.1 HAC', 'r9.4.1 SUP',
            'r10.3 Rerio','r10.3 SUP',
            'r10.4 HAC', 'r10.4 SUP','r10.4 DUPLEX',
            'r10.4.1 SUP','r10.4.1 DUPLEX',
            'r9.4.1+Illumina', 'Illumina']

    ax2=sns.barplot(x='Species', y='Percentage of genes found',
            hue='FC+Model',data=g2,
            hue_order=hue_order)
    plt.legend(loc='lower right')
    #ax2.set_ylim(80,100)
    plt.tight_layout()
    plt.savefig('figs/genes_found_flowcell.png')
    plt.savefig('figs/genes_found_flowcell.pdf')
    plt.savefig('figs/genes_found_flowcell.svg')
    plt.show()
    plt.clf()
    
    ax2=sns.barplot(x='Species', y='Percentage of names matching',
            hue='FC+Model',data=g2,
            hue_order=hue_order)
    #ax2.set_ylim(50,100)
    plt.legend(loc='lower right')
    plt.tight_layout()
    plt.savefig('figs/names_matches_flowcell.png')
    plt.savefig('figs/names_matches_flowcell.pdf')
    plt.savefig('figs/genes_matches_flowcell.svg')
    plt.show()
    plt.clf()

    ax3=sns.catplot(x='subSample', y='Percentage of genes found',
            hue='FC+Model',data=g1,
            kind='bar',
            col='Species',
            row='Assembler',
            hue_order=hue_order)
    #ax3.set_ylim(40,100)
    plt.savefig('figs/genes_found_flowcell_subsample.png')
    plt.savefig('figs/genes_found_flowcell_subsample.pdf')
    plt.savefig('figs/genes_found_flowcell_subsample.svg')
    plt.show()
    plt.clf()


def snippyStats():
    dfs=[]
    for bug in ['ecoli',
        'kpneumo',
        'MRSA',
        'Pa01']:
        df=pd.read_csv('snippy/{0}.txt'.format(bug),
                names=['feat','value'],
                sep='\t')
        df=df.set_index('feat')
        df2=df.T#.reset_index()
        df2['Bug']=bug
        dfs.append(df2)
    df=pd.concat(dfs)
    df['Species']=df['Bug'].map(bugDict)

    df=df[['Species','Variant-COMPLEX','Variant-DEL','Variant-INS','Variant-MNP','Variant-SNP','VariantTotal']]
    df.to_csv('CSVs/snippy_results_summary.csv',index=False)
    print(df)


def runTimes():
    df=pd.read_csv('runtimes_sampled/all_sampled.csv')
    df['run time']=pd.to_timedelta(df['run time'])
    #df['seconds']=df['run time'].dt.total_seconds()
    #df['Hours']=df['seconds']/3600
    df.sort_values(by=['name','seconds'],inplace=True)
    df['Cumulative gigabases']=df['cumulative bases']/1000000000
    df['total bases']=df.groupby(['name','runid'])[['cumulative bases']].transform(max)
    df['mean length']=df.groupby(['name','runid'])[['seq_len']].transform('mean')
    df['median length']=df.groupby(['name','runid'])[['seq_len']].transform('median')
    df['% total bases']=(df['cumulative bases']/df['total bases'])*100
    df=df.apply(getSampleDetails,axis=1)
    print(df['Cumulative gigabases'].max())
    df.to_csv('runtimes_sampled/all_sampled_meta.csv')

def runTimesPlot():
    df=pd.read_csv('runtimes_sampled/all_sampled_meta.csv')
    #df=df[df['plexed']==False]
    df=df[df['Flowcell']=='r10.4']
    df=df[df['Model'].isin(['SUP','DUPLEX'])]

    # plots
    ax1=sns.relplot(x='Hours', y='% total bases',
            col='parent', col_wrap=4,
            hue='Model',
            kind="line",
            data=df)

    plt.show()
    plt.clf()

    #total bases
    df2=df.drop_duplicates(subset=['name'],keep='last')
    ax2=sns.barplot(y='parent', x='Cumulative gigabases',
            data=df2,
            hue='Model',
            log=True)
    plt.show()
    plt.clf()

    # proportion duplex
    df2['total parent bases']=df2.groupby(['parent'])[['cumulative bases']].transform(max)
    df2['% duplex bases']=(df2['cumulative bases']/df2['total parent bases'])*100
    df3=df2[df2['Model']=='DUPLEX']
    ax3=sns.barplot(y='parent', x='% duplex bases',
            data=df3)
    plt.tight_layout()
    plt.show()
    plt.clf()

    # duplex over mean length
    ax4=sns.scatterplot(x='mean length', y='% duplex bases',
            data=df3,
            hue='Species')
    plt.show()
    plt.clf()
    # duplex over median length
    ax5=sns.scatterplot(x='median length', y='% duplex bases',
            data=df3,
            hue='Species')
    plt.show()
    plt.clf()

def plasmids():
    names=['start ref', 'end ref', 'start query', 'end query','aln len ref', 'aln len query',
            '% IDY', 'len ref', 'len query', 'cov ref', 'cov query', 'ref ID', 'query ID'
            ]
    l=[]
    for assembler in ['canu', 'flye','flye_hq', 'spades', 'unicycler']:
        coords=os.listdir('dnadiff/{0}'.format(assembler))
        coords=[c for c in coords if c.endswith('.1coords')]
        for coord in coords:
            df=pd.read_csv('dnadiff/{0}/{1}'.format(assembler, coord),
                    sep='\t',
                    names=names)
            df['assembler']=assembler
            df['sample name']=coord.replace('.1coords','')
            df['Assembly']=df['sample name'] + '_' + df['assembler']
            l.append(df)
    df=pd.concat(l)

    # ref % represented
    df['Total ref cov']=df.groupby(['Assembly','ref ID'])['cov ref'].transform(sum)

    # number of unique contigs
    df['No. unique contigs']=df.groupby(['Assembly','ref ID'])['query ID'].transform('nunique')

    # added meta data and save
    df=df.apply(getAssemblyDetails,axis=1)
    df.to_csv('CSVs/dnadiff.1coords.csv',index=False)

def plot_plasmids():
    df=pd.read_csv('CSVs/dnadiff.1coords.csv')
    # Plots and tables
    df2=df.drop_duplicates(subset=['Assembly','ref ID'])
    df2=df2[['Assembly','Species', 'Flowcell', 'Model',
            'Assembler', 'subSample', 'plexed',
            'ref ID','len ref', 'Total ref cov',	'No. unique contigs']]
    df2.to_csv('CSVs/dnadiff_ref_recovery.csv',index=False)

    # Only Kleb plasmids
    df2=df2[df2['Species']=='K.pneumoniae']
    df2=df2[df2['ref ID']!='CP000647.1']
    df2.to_csv('CSVs/dnadiff_plasmid_recovery.csv',index=False)

    # pivot
    indexes=['Assembly', 'Species', 'Flowcell', 'Model', 'Assembler','subSample','plexed']
    df3=df2.pivot_table(index=indexes,
            columns=['ref ID'],
            values=['Total ref cov','No. unique contigs'])
    df3.to_csv('CSVs/dnadiff_plasmid_recovery_pivot.csv')

def run():
    #runTimes()
    #runTimesPlot()
    raw_sequencing_stats()
    raw_read_stats()
    #get_assembly_stats()
    #get_assembly_QUAST_stats()
    get_assembly_DNAdiff_stats()
    #get_assembly_Blast_stats()
    prokkaGenes()
    #snippyStats()
    #geneFeatures()
    #plasmids()
    #plot_plasmids()
    #variant_callingSNPs()
    #variant_callingINDELs()

if __name__ == '__main__':
    run()


