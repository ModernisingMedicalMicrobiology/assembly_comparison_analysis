#!/usr/bin/env python3
from xml.dom import minidom
import xml.etree.cElementTree as ET
import pandas as pd
import sys


def indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


def makeExperimentXML(df):
    experiment_set = ET.Element('EXPERIMENT_SET')
    for index,row in df.iterrows():
        alias=row['ALIAS']
        accession=row['ACCESSION']
        refname=row['Study']
        library_strategy=row['library_strategy']
        library_selection=row['library_selection']
        library_source=row['library_source']
        instrument_model=row['instrument_model']
        center_name=row['center_name']
    
        ## XML
        experiment = ET.SubElement(experiment_set, 'EXPERIMENT', alias=alias)
        study_ref = ET.SubElement(experiment, "STUDY_REF", accession=refname)
        
        # indent
        design = ET.SubElement(experiment, "DESIGN")
        
        # indent
        design_description = ET.SubElement(design, "DESIGN_DESCRIPTION")
        sample_descriptor = ET.SubElement(design, 'SAMPLE_DESCRIPTOR', accession=accession)
        library_descriptor = ET.SubElement(design, "LIBRARY_DESCRIPTOR")
        library_name = ET.SubElement(library_descriptor, "LIBRARY_NAME")
        library_strategy = ET.SubElement(library_descriptor, "LIBRARY_STRATEGY").text = str(library_strategy)
        library_source = ET.SubElement(library_descriptor, "LIBRARY_SOURCE").text = str(library_source)
        library_selection = ET.SubElement(library_descriptor, "LIBRARY_SELECTION").text = str(library_selection)
        library_layout_dir = ET.SubElement(library_descriptor, "LIBRARY_LAYOUT")
        #indent
        paired = ET.SubElement(library_layout_dir, "SINGLE") 
        
        # dedent
        platform = ET.SubElement(experiment, "PLATFORM")
        ont = ET.SubElement(platform, "OXFORD_NANOPORE")
        
        #indent
        instrument_model = ET.SubElement(ont, "INSTRUMENT_MODEL").text = str(instrument_model)
        
        # dedent
        #processing = ET.SubElement(experiment, "PROCESSING")
    
    # use the indent function to indent the xml file
    indent(experiment_set)
    # create tree
    tree = ET.ElementTree(experiment_set)
    
    # out_dirput to outfile
    with open("experiment.xml", 'wb') as outfile:
    	tree.write(outfile, xml_declaration=True, encoding='utf-8', method="xml")


def makeRunXML(df):
    run_set = ET.Element('RUN_SET')
    for index,row in df.iterrows():
        alias=row['ALIAS']
        file=row['file']
        exp_ref=row['ALIAS']
        f=row['file']

        ## XML
        run = ET.SubElement(run_set, 'RUN', alias=alias)
        experiment_ref = ET.SubElement(run, "EXPERIMENT_REF", refname=exp_ref)

        # indent
        data_block = ET.SubElement(run, "DATA_BLOCK")

        # indent
        files = ET.SubElement(data_block, "FILES")
        file = ET.SubElement(files,"FILE", filename=file, filetype="OxfordNanopore_native",
                checksum_method="MD5", checksum=row['md5'])

    # use the indent function to indent the xml file
    indent(run_set)
    # create tree
    tree = ET.ElementTree(run_set)

    # out_dirput to outfile
    with open("run.xml", 'wb') as outfile:
        tree.write(outfile, xml_declaration=True, encoding='utf-8', method="xml")

# read df and run 
df=pd.read_csv(sys.argv[1])
makeExperimentXML(df)
makeRunXML(df)
