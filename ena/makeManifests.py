#!/usr/bin/env python3
import pandas as pd

df=pd.read_csv('Webin-accessions-2022-02-25T13_58_29.441Z.txt',sep='\t')
df=df[df['TYPE']!='SUBMISSION']

df['STUDY']='PRJEB51164'
df['NAME']=df['ALIAS']
df['SAMPLE']=df['ACCESSION']
df['PLATFORM']='OXFORD_NANOPORE'
df['INSTRUMENT']='GridION'
df['LIBRARY_SOURCE']='GENOMIC'
df['LIBRARY_SELECTION']='PCR'
df['LIBRARY_STRATEGY']='WGS'
df['DESCRIPTION']='NA'
df['FASTQ']=df['ALIAS']+'.fastq.gz'

df=df[['STUDY', 'SAMPLE', 'NAME', 'PLATFORM', 'INSTRUMENT', 'LIBRARY_SOURCE','LIBRARY_SELECTION', 'LIBRARY_STRATEGY', 'DESCRIPTION', 'FASTQ']]

print(df)
df2=df.T#.reset_index()
for i in df2.columns:
    print(df2[i]['NAME'])
    df3=df2[i].reset_index()
    df3.to_csv('manifests/{0}.tsv'.format(df2[i]['NAME']),
            index=False,
            sep='\t')

